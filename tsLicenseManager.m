//
//  tsLicenseManager.m
//  TypoStudio
//
//  Created by 낡은피아노 on 12. 10. 18..
//
//

#import <unistd.h>
#import <time.h>
#import <sys/stat.h>
#import <CommonCrypto/CommonDigest.h>
#import "tsLicenseManager.h"

//#define DEBUG

int	 access(const char *, int);
time_t time(time_t *);

#define NSStringFromC(s) [NSString stringWithCString:s encoding:NSUTF8StringEncoding]

static BOOL tsLicenseRecheck = NO;

void arr2char(char **ret, const int *arr, const size_t size) {
	char *s = malloc(size + 1);
	for (int i = 0; i < size; i++) {
		s[i] = arr[i];
	}
	s[size] = '\0';
	*ret = s;
}

void tsLicenseCheck(const char *filename, bool check, void (^handler)(BOOL, BOOL))
{
	const int pkgname[] = tsPackageName;
	const int pkglist[] = tsPackageListFile;
	const int licfile[] = tsPackageLicenseFile;

	char *name, *file;
	name = NULL;
	file = NULL;

	struct stat sb;
	int st;
	if (filename != NULL) {
		st = stat(filename, &sb);
		if (st != 0 || (st == 0 && sb.st_mtime > tsPackageTime)) {
#ifdef DEBUG
			NSLog(@"!!!!!!! BUNDLE TIME !!!!!!!!!!!!!!!!!!!!!!");
#endif
			if (handler) handler(NO, NO);
			goto exit;
		}
	}
	
	arr2char(&file, pkglist, sizeof(pkglist)/sizeof(pkglist[0]));
	if (access(file, R_OK) != 0) {
#ifdef DEBUG
		NSLog(@"!!!!!!! PKGLIST !!!!!!!!!!!!!!!!!!!!!!");
#endif
		if (handler) handler(NO, NO);
		goto exit;
	}
	
	arr2char(&name, pkgname, sizeof(pkgname)/sizeof(pkgname[0]));

	const char *uuid = [[[UIDevice currentDevice] uniqueIdentifier] UTF8String];
	char *cStr; asprintf(&cStr, "%s %s", name, uuid);
	unsigned char digest[CC_MD5_DIGEST_LENGTH];
	CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
	
	NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
	
	for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
		[output appendFormat:@"%02x", digest[i]];
	}
	
	free(cStr);
	
	//char *file;
	arr2char(&file, licfile, sizeof(licfile)/sizeof(licfile[0]));
	NSString *licensefile = [NSString stringWithCString:file encoding:NSASCIIStringEncoding];
	NSString *license = [NSString stringWithContentsOfFile:licensefile encoding:NSUTF8StringEncoding error:nil] ?: @"";

	st = stat(file, &sb);
	time_t t; time(&t);
	if (st == 0 && t - sb.st_mtime < 3600) {
#ifdef DEBUG
		NSLog(@"!!!!!!! LICENSE TIME !!!!!!!!!!!!!!!!!!!!!!");
#endif
		if (handler) handler([output isEqualToString:license], YES);
		goto exit;
	}

	//NSString *s = tsPackageLicenseData;
	
	if (tsLicenseRecheck || (check && [license isEqualToString:@""])) {
		const int licarr[] = tsLicenseCheckURL;
		char *licurl; arr2char(&licurl, licarr, sizeof(licarr)/sizeof(licarr[0]));
		NSString *url = [NSString stringWithCString:licurl encoding:NSASCIIStringEncoding];
		url = [NSString stringWithFormat:url, name, tsPackageVersion, time(NULL), uuid];
		
		free(licurl);

		NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
		[NSURLConnection sendAsynchronousRequest:request
										   queue:[NSOperationQueue currentQueue]
							   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
		 {
			 NSString *d = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
			 [data writeToFile:licensefile atomically:YES];
			 if ([d isEqualToString:output]) {
				 if (handler) handler(YES, YES);
			 }
			 else if (handler) {
#ifdef DEBUG
				 NSLog(@"!!!!!!! NONE LICENSE !!!!!!!!!!!!!!!!!!!!!!");
#endif
				 handler(NO, NO);
			 }
		 }];
	}
	else if ([output isEqualToString:license]) {
		if (handler) handler(YES, YES);
	}
	else {
#ifdef DEBUG
		NSLog(@"!!!!!!! NO LICENSE !!!!!!!!!!!!!!!!!!!!!!");
#endif
		if (handler) handler(NO, YES);
	}
	
exit:
	tsLicenseRecheck = NO;

	free(name);
	if (file) free(file);
}

BOOL tsLicenseCheck2(const char *package)
{
	return [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"/var/lib/dpkg/info/%s.list", package]];
}