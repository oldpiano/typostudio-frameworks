//
//  tsSettingsController.m
//  
//
//  Created by 낡은피아노 on 13. 4. 8..
//
//

#import "tsSettingsController.h"

@implementation tsSettingsController

- (NSString *)localizedStringForKey:(NSString *)key
{
	return [self.bundle localizedStringForKey:key value:nil table:@"tsSpeeder"];
}

- (id)loadSpecifiersFromPlistName:(NSString *)plistName target:(id)target
{
	id specs = [super loadSpecifiersFromPlistName:plistName target:target];
	
	if (_specifiers == nil) _specifiers = [specs retain];

	PSSpecifier *sp = [self specifierForID:@"version"];
	[sp setProperty:[NSString stringWithFormat:[sp propertyForKey:@"footerText"], tsPackageVersion, [[[NSCalendar currentCalendar] components:NSYearCalendarUnit fromDate:[NSDate date]] year]] forKey:@"footerText"];

	tsBugReporter *reporter = [tsBugReporter sharedReporter];
	reporter.bundle = self.bundle;

	return _specifiers;
}

- (id)specifierForID:(id)id_
{
	id sp = [super specifierForID:id_];
	if (!sp) [self removeLastSpecifier];
	return sp;
}

- (void)removeSpecifierID:(id)anId animated:(BOOL)animated
{
	if ([super specifierForID:anId]) {
		[super removeSpecifierID:anId animated:animated];
	}
}

- (void)twitter:(PSSpecifier *)specifier
{
	UIActionSheet *sheet = [[UIActionSheet alloc] init];
	sheet.title = @"@typ0s2d10";
	sheet.delegate = self;
	
	UIApplication *app = [UIApplication sharedApplication];
	if ([app canOpenURL:[NSURL URLWithString:@"netbot://"]]) {
		[sheet addButtonWithTitle:@"NetBot"];
	}
	if ([app canOpenURL:[NSURL URLWithString:@"tweetbot://"]]) {
		[sheet addButtonWithTitle:@"TweetBot"];
	}
	if ([app canOpenURL:[NSURL URLWithString:@"twitter://"]]) {
		[sheet addButtonWithTitle:@"Twitter"];
	}
	
	[sheet addButtonWithTitle:@"Safari"];
	int c = [sheet addButtonWithTitle:[self localizedStringForKey:@"Cancel"]];
	sheet.cancelButtonIndex = c;
	[sheet showInView:[(UIViewController *)self view]];
	[sheet release];
}

- (void)actionSheet:(UIActionSheet *)sheet clickedButtonAtIndex:(int)index
{
	if (index == sheet.cancelButtonIndex) {
		return;
	}
	
	NSString *title = [sheet buttonTitleAtIndex:index];
	
	if (![sheet.title isEqualToString:@"@typ0s2d10"]) return;
	
	UIApplication *app = [UIApplication sharedApplication];
	if ([title isEqualToString:@"Safari"]) {
		[app openURL:[NSURL URLWithString:@"https://mobile.twitter.com/typ0s2d10"]];
	}
	else if ([title isEqualToString:@"NetBot"]) {
		[app openURL:[NSURL URLWithString:@"netbot:///user_profile/typ0s2d10"]];
	}
	else if ([title isEqualToString:@"TweetBot"]) {
		[app openURL:[NSURL URLWithString:@"tweetbot:///user_profile/typ0s2d10"]];
	}
	else if ([title isEqualToString:@"Twitter"]) {
		[app openURL:[NSURL URLWithString:@"twitter:///user?screen_name=typ0s2d10"]];
	}
}

- (void)viewWillAppear:(BOOL)animated
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UITableView *tableView = (UITableView *)self.table;
	if ([tableView indexPathForSelectedRow]) {
		[tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
	}	
}

- (void)viewDidAppear:(BOOL)animated
{
	if (![self specifierForID:@"twitter"]) {
		CFUserNotificationDisplayAlert(0, 0, NULL, NULL, NULL, NULL, (CFStringRef)[self localizedStringForKey:@"Don't modify plist!"], CFSTR("OK"), NULL, NULL, NULL);
		[self.rootController popViewControllerAnimated:YES];
		return;
	}
}

- (void)willBecomeActive
{
	[self viewWillAppear:NO];
}

- (void)willResignActive
{
	[self viewWillAppear:NO];
}

- (void)showHelp:(PSSpecifier *)specifier
{
	helpViewed = NO;
	
	UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,0,0,0)];
	webView.delegate = self;
	webView.backgroundColor = [UIColor clearColor];
	[[[[webView.subviews lastObject] subviews] lastObject] setBackgroundColor:[UIColor clearColor]];
	
	
	UIViewController *webViewController = [[UIViewController alloc] init];
	webViewController.title = [specifier name];
	webViewController.view.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:1.0f];
	[webViewController.view addSubview:webView];
	
	UIBarButtonItem *button = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:nil action:nil] autorelease];
	webViewController.navigationItem.rightBarButtonItem = button;
	
	tsBugReporter *reporter = [tsBugReporter sharedReporter];
	reporter.barButton = button;
	reporter.navigationController = (UINavigationController *)self;
	reporter.webView = webView;

	if ([self respondsToSelector:@selector(pushController:animate:)]) {
		[self pushController:webViewController animate:YES];
	}
	else {
		[(UINavigationController *)self pushViewController:webViewController animated:YES];
	}
	[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:tsPackageHelpURL]]];
	
	[webView release];
	[webViewController release];
}

- (void)report:(PSSpecifier *)specifier
{
	tsBugReporter *reporter = [tsBugReporter sharedReporter];
	reporter.webView = nil;
	reporter.barButton = nil;
	reporter.navigationController = (UINavigationController *)self;
	[reporter showMailComposer];
}

#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	if (!helpViewed) {
		webView.alpha = 0.0f;
		webView.frame = webView.superview.bounds;
		
		[UIView animateWithDuration:1.6 delay:0.4 options:0 animations:^{
            webView.alpha = 1.0;
        } completion:NULL];
	}
	
	helpViewed = YES;
}


@end


@implementation UIViewController (typ0s2d10)

- (void)setRootController:(id)controller
{
}

@end
