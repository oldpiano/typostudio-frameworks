//
//  tsBugReporter.m
//  TypoStudio
//
//  Created by 낡은피아노 on 12. 10. 15..
//  Copyright (c) 2012년 TypoStudio. All rights reserved.
//

#import "tsBugReporter.h"
#import "tsHudManager.m"
#import <notify.h>
//#import "UIViewController+KNSemiModal.h"

#define NSStringFromC(s) [NSString stringWithCString:s encoding:NSUTF8StringEncoding]

static tsBugReporter *bugReporter;

@implementation tsBugReporter

@synthesize bundle, bundleTableName, attachments, crashReportFile;
@synthesize barButton, navigationController, webView;

+ (id)sharedReporter
{
	if (!bugReporter) {
		bugReporter = [[tsBugReporter alloc] init];
	}
	
	return bugReporter;
}

- (void)setBarButton:(UIBarButtonItem *)button
{
	button.target = self;
	button.action = @selector(showActionSheet:);
	barButton = button;
}

- (id)init
{
	self = [super init];
	
	bundle = [NSBundle mainBundle];
	bundleTableName = @"tsBugReporter";
	
	return self;
}

- (void)dealloc
{
	[crashReportFile release];
	[attachments release];
	[bundleTableName release];
	[super dealloc];
}

- (NSString *)localizedStringForKey:(NSString *)key
{
	return [bundle localizedStringForKey:key value:nil table:bundleTableName];
}

- (void)show
{
	if (![MFMailComposeViewController canSendMail]) {
		UIAlertView *alert = [[UIAlertView alloc] init];
		alert.message = [self localizedStringForKey:@"No Mail Account!"];
		[alert addButtonWithTitle:@"OK"];
		[alert show];
		[alert release];
		return;
	}
	
	[UIProgressHUD show:NULL inView:[[UIApplication sharedApplication] keyWindow]];
	
	NSFileManager *fm = [NSFileManager defaultManager];
	self.crashReportFile = [fm destinationOfSymbolicLinkAtPath:tsBugReporterCrashReporter error:nil];
	self.crashReportFile = [[tsBugReporterCrashReporter stringByDeletingLastPathComponent] stringByAppendingPathComponent:self.crashReportFile];

	if ([fm fileExistsAtPath:kAshikaseSymbolicate] &&
		[fm fileExistsAtPath:self.crashReportFile]) {
		NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(checkSymbolicated:) userInfo:nil repeats:YES];
		[[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
		[timer fire];
		
		int token;
		
		notify_register_dispatch("jp.ashikase.symbolicate.progress", &token, dispatch_get_main_queue(), ^(int t) {
			uint64_t status;
			notify_get_state(t, &status);
			[UIProgressHUD setText:[NSString stringWithFormat:@"%d %%", (int)status]];
			
			[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showMailComposer) object:nil];
			[self performSelector:@selector(showMailComposer) withObject:nil afterDelay:10.0];
		});
		
		[self performSelector:@selector(showMailComposer) withObject:nil afterDelay:20.0];

		NSString *command = [NSString stringWithFormat:@"%1$@ -n 1 -o %2$@.synced %2$@ & 2>&1", kAshikaseSymbolicate, self.crashReportFile, nil];
//		NSLog(@"%@", command);
		int ret = system([command UTF8String]);
		if (ret == -1) {
			[self showMailComposer];
		}
	}
	else {
		[self showMailComposer];
	}
}

- (void)checkSymbolicated:(NSTimer *)timer
{
	if ([[NSFileManager defaultManager] fileExistsAtPath:[self.crashReportFile stringByAppendingPathExtension:@"synced"]]) {
		[timer invalidate];
		[self performSelector:@selector(showMailComposer)];
	}
}

- (void)showMailComposer
{
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showMailComposer) object:nil];
	
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	controller.mailComposeDelegate = self;
	
	UIDevice *device = [UIDevice currentDevice];
	
	[controller setSubject:[NSString stringWithFormat:[self localizedStringForKey:@"%@ %@ Bug Reporting"], NSStringFromC(tsPackageTitle), NSStringFromC(tsPackageVersion)]];
	[controller setToRecipients:[NSArray arrayWithObject:tsBugReporterDevMail]];
	[controller setMessageBody:[NSString stringWithFormat:[self localizedStringForKey:@"OS : %@ %@\nUDID : %@\nProblems : \n\n\n"],
								[device platformString],
								[device systemVersion],
								[device uniqueIdentifier], nil]
				  isHTML:NO];

	NSFileManager *fm = [NSFileManager defaultManager];
	
	system([[NSString stringWithFormat:@"dpkg -l > %@", tsBugReporterLogFileDPKG] UTF8String]);
	if ([fm fileExistsAtPath:tsBugReporterLogFileDPKG]) {
		[controller addAttachmentData:[NSData dataWithContentsOfFile:tsBugReporterLogFileDPKG]
					   mimeType:@"text/plain"
					   fileName:[tsBugReporterLogFileDPKG lastPathComponent]];
	}
	
	if ([fm fileExistsAtPath:[self.crashReportFile stringByAppendingPathExtension:@"synced"]]) {
		self.crashReportFile = [self.crashReportFile stringByAppendingPathExtension:@"synced"];
	}
	
	if ([fm fileExistsAtPath:self.crashReportFile]) {
		[controller addAttachmentData:[NSData dataWithContentsOfFile:self.crashReportFile] mimeType:@"text/plain"
							 fileName:[self.crashReportFile lastPathComponent]];
	}
	
	for (NSString *path in attachments) {
		BOOL isDir;
		if ([fm fileExistsAtPath:path isDirectory:&isDir] && isDir) {
			NSError *error = nil;
			NSArray *list = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:[NSURL fileURLWithPath:path] includingPropertiesForKeys:[NSArray arrayWithObject:NSURLContentModificationDateKey] options:0 error:&error];
			if (error) continue;

			NSMutableDictionary *urlWithDate = [NSMutableDictionary dictionaryWithCapacity:list.count];
			for (NSURL *f in list) {
			    NSDate *date;
			    if ([f getResourceValue:&date forKey:NSURLContentModificationDateKey error:&error]) {
			        [urlWithDate setObject:date forKey:f];
			    }
			}

			list = [urlWithDate keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2) {
			    return [obj2 compare:obj1];
			}];
			for (int i = 0; i < MIN(3, list.count); i++) {
				NSURL *url = [list objectAtIndex:i];
				[controller addAttachmentData:[NSData dataWithContentsOfURL:url]
									 mimeType:@"text/plain"
									 fileName:[url lastPathComponent]];
			}
		}
		else if ([fm fileExistsAtPath:path]) {
			[controller addAttachmentData:[NSData dataWithContentsOfFile:path]
								 mimeType:@"text/plain"
								 fileName:[path lastPathComponent]];
		}
	}

	[UIProgressHUD close];
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
		controller.modalInPopover = YES;
		controller.modalPresentationStyle = UIModalPresentationFormSheet;
	}

	if ([navigationController respondsToSelector:@selector(presentViewController:animated:completion:)]) {
		[navigationController presentViewController:controller animated:YES completion:NULL];
	}
	else if ([navigationController respondsToSelector:@selector(presentModalViewController:animated:)]) {
		[navigationController presentModalViewController:controller animated:YES];
	}
	
	[controller release];
}

#pragma mark MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	[controller dismissModalViewControllerAnimated:YES];
}

#pragma mark UIActionSheet

- (void)showActionSheet:(id)sender
{
	UIActionSheet *sheet = [[[UIActionSheet alloc] init] autorelease];
	sheet.delegate = self;
	sheet.title = [NSString stringWithFormat:@"%s %s ©typ0s2d10", tsPackageTitle, tsPackageVersion];
	
	if (webView) {
		[sheet addButtonWithTitle:[self localizedStringForKey:@"Open via Safari"]];
	}
	
	[sheet addButtonWithTitle:[self localizedStringForKey:@"Bug Report"]];
	[sheet addButtonWithTitle:[self localizedStringForKey:@"Cancel"]];
	
	sheet.destructiveButtonIndex = sheet.numberOfButtons - 2;
	sheet.cancelButtonIndex = sheet.numberOfButtons - 1;
	
	[sheet showFromBarButtonItem:sender animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == actionSheet.numberOfButtons - 2) {
		[self showMailComposer];
	}
	else if (buttonIndex == actionSheet.numberOfButtons - 3) {
		[[UIApplication sharedApplication] openURL:webView.request.URL];
	}
}

@end
