//
//  tsHudManager.m
//  TypoStudio
//
//  Created by 낡은피아노 on 12. 10. 15..
//  Copyright (c) 2012년 TypoStudio. All rights reserved.
//

//#import <UIKit/UIProgressHUD.h>


@class UIImageView, UILabel, UIProgressIndicator, UIWindow;

@interface UIProgressHUD : UIView {
    struct {
        unsigned int isShowing : 1;
        unsigned int isShowingText : 1;
        unsigned int fixedFrame : 1;
        unsigned int reserved : 30;
		UIImageView *_doneView;
		UIWindow *_parentWindow;
    } _progressHUDFlags;
    UIProgressIndicator *_progressIndicator;
    UILabel *_progressMessage;
}

- (id)_progressIndicator;
- (void)dealloc;
- (void)done;
- (void)drawRect:(struct CGRect)arg1;
- (void)hide;
- (id)initWithFrame:(struct CGRect)arg1;
- (id)initWithWindow:(id)arg1;
- (void)layoutSubviews;
- (void)setFontSize:(int)arg1;
- (void)setShowsText:(BOOL)arg1;
- (void)setText:(NSString *)arg1;
- (void)show:(BOOL)arg1;
- (void)showInView:(id)arg1;

@end

static UIProgressHUD *hud = nil;

@interface UIProgressHUD (typ0s2d10)
+ (void)show:(NSString *)msg inView:(UIView *)view;
+ (void)close;
@end

@implementation UIProgressHUD (typ0s2d10)

+ (void)show:(NSString *)msg inView:(UIView *)view
{
	hud = [[UIProgressHUD alloc] init];
	[hud setText:msg];
	[hud showInView:view];
}

+ (void)close
{
	if (hud == nil) return;
	 [hud hide];
	 [hud removeFromSuperview];
	 [hud release];
	 hud = nil;
}

+ (void)setText:(NSString *)msg
{
	[hud setText:msg];
}

@end