//
//  tsSettingsController.h
//  
//
//  Created by 낡은피아노 on 13. 4. 8..
//
//

#import <Preferences/Preferences.h>
#include "tsBugReporter.h"
#include "tsLicenseManager.h"

SInt32 CFUserNotificationDisplayAlert (
									   CFTimeInterval timeout,
									   CFOptionFlags flags,
									   CFURLRef iconURL,
									   CFURLRef soundURL,
									   CFURLRef localizationURL,
									   CFStringRef alertHeader,
									   CFStringRef alertMessage,
									   CFStringRef defaultButtonTitle,
									   CFStringRef alternateButtonTitle,
									   CFStringRef otherButtonTitle,
									   CFOptionFlags *responseFlags
									   );

@interface tsSettingsController : PSListController <UIWebViewDelegate, UIActionSheetDelegate>
{
    BOOL helpViewed;
}

- (NSString *)localizedStringForKey:(NSString *)key;

- (void)viewWillAppear:(BOOL)animated;
- (void)showHelp:(PSSpecifier *)specifier;
- (void)report:(PSSpecifier *)specifier;

@end
